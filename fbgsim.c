#include "fbgsim.h"

#include <math.h>
#include <complex.h>
#include <stdlib.h>

/**
 * J or transfer-matrix. Private.
 */
typedef struct {
    float complex j11, j12, j21, j22;
} FBGJMatrix;

const float pi = 3.14159265358979323846;

FBG* fbg_new(int num_segments) {
    if(num_segments < 1) return NULL;
    
    FBG* fbg = malloc(sizeof(FBG));
    if(!fbg) return NULL;
    
    fbg->segments = malloc( num_segments*sizeof(FBGSegment) );
    if(!fbg->segments) {
        free(fbg);
        return NULL;
    }
    
    fbg->num_segments = num_segments;
    return fbg;
}

void fbg_free(FBG* fbg) {
    free(fbg->segments);
    free(fbg);
}

float fbg_length(const FBG* fbg) {
    float len = 0;
    
    for(int i = 0; i < fbg->num_segments; ++i)
        len += fbg->segments[i].length;
    
    return len;
}

float fbg_ideal_length(float desired, float Lambda, int num_segments) {
    float aux  = Lambda*num_segments;
    int seg_mul = ceilf(desired/aux);
    return aux*seg_mul;
}

FBG* fbg_uniform(float length, float Lambda, float n_av, float delta_n0, int num_segments) {
    FBG* fbg = fbg_new(num_segments);
    if(!fbg) return NULL;
    
    for(int i = 0; i < num_segments; ++i) {
        FBGSegment* seg = fbg->segments+i;
        seg->length     = length / num_segments;
        seg->Lambda     = Lambda;
        seg->n_av       = n_av;
        seg->delta_n0   = delta_n0;
    }
    
    return fbg;
}

void fbg_linear_chirp(FBG* fbg, float delta_Lambda, int preserve) {
    float seg_length = fbg->segments->length;
    float Lambda0    = fbg->segments->Lambda;
    float factor     = seg_length / Lambda0;
    
    for(int i = 0; i < fbg->num_segments; ++i) {
        float z         = seg_length*(i+0.5);
        FBGSegment* seg  = fbg->segments+i;
        float inc       = delta_Lambda / Lambda0 * z;
        seg->Lambda += inc;
        
        if(preserve) {
            seg->length = fbg_ideal_length(seg->length, seg->Lambda, 1);
        } else {
            seg->length += inc*factor;
        }
    }
}

void fbg_apod_gaussian(FBG* fbg, float a, float y) {
    int num_segments  = fbg->num_segments;
    float length      = fbg_length(fbg);
    float cur_length  = 0.f;
    
    for(int i = 0; i < num_segments; ++i) {
        FBGSegment* seg   = fbg->segments+i;
        float z           = cur_length + seg->length/2.f;
        cur_length += seg->length;
        
        float aux = seg->delta_n0*exp(-a*pow( (z-length/2.f)/length, 2.f) );
        seg->n_av       += y*aux;
        seg->delta_n0    = aux;
    }
}
 
FBGJMatrix fbg_jmatrix(const FBGSegment* seg, float lambda) {
    float Beta       = 2*seg->n_av*pi/lambda;
    float Beta0      = pi/seg->Lambda;
    float delta_Beta = Beta - Beta0;
    float k          = pi*seg->delta_n0/lambda;
    float complex s  = csqrtf( powf(cabsf(k),2) - powf(delta_Beta,2));
    float l          = seg->length;
    
    FBGJMatrix J;
    J.j11 = ( delta_Beta*csinhf(s*l) + I*s*ccoshf(s*l) ) / (I*s);
    J.j12 = k*csinhf(s*l) / (I*s);
    J.j21 = conjf(J.j12);
    J.j22 = conjf(J.j11);
    
    return J;
}

float fbg_sim(const FBG* fbg, float lambda) {
    float complex a = 0.0, b = 1.0;
    
    for(int i = fbg->num_segments-1; i >= 0; --i) {
        const FBGSegment* seg = &fbg->segments[i];
        FBGJMatrix J    = fbg_jmatrix(seg, lambda);
        
        float complex an = J.j11*a + J.j12*b;
        float complex bn = J.j21*a + J.j22*b;
        a = an;
        b = bn;
    }
    
    return powf(cabsf(a/b), 2);
}
