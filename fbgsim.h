#ifndef FBGSIM_H
#define FBGSIM_H

/*
 * Simulation based on:
 * 
 * S. Huang, M. LeBlanc, M. Ohn, and R. Measures, "Bragg intragrating
 * structural sensing", Appl. Opt.  34, 5003-5009 (1995).
 * 
 * More precision can be achieved by using doubles instead of floats,
 * and by using thir respective functions (csinh instead of csinhf).
 */

/**
 * Parameters of an FBG segment.
 */
typedef struct {
    float length;   // [nm]
    float Lambda;   // [nm] 
    float n_av;     // [dimensionless]
    float delta_n0; // [dimensionless]
} FBGSegment;

/**
 * An FBG can be modeled as 'one segment' (uniform case) or as
 * a sequence of segments for nonuniform cases.
 */
typedef struct {
    FBGSegment* segments;
    int num_segments;
} FBG;

// all length values are in  nanometers by default
// 5*FBG_CM corresponds to 5cm or 5x10^7nm
#define FBG_NM 1f
#define FBG_CM 1e7f

/* functions */

FBG*  fbg_new          (int num_segments);
void  fbg_free         (FBG* fbg);
float fbg_length       (const FBG* fbg);
float fbg_ideal_length (float desired, float Lambda, int num_segments);
FBG*  fbg_uniform      (float length, float Lambda, float n_av, float delta_n0, int num_segments);
void  fbg_linear_chirp (FBG* fbg, float delta_Lambda, int preserve);
void  fbg_apod_gaussian(FBG* fbg, float a, float y);
float fbg_sim          (const FBG* fbg, float lambda);

#endif
