CFLAGS  = -O3 -ffast-math -march=native -flto -std=c99 -Wall -fopenmp
LDFLAGS = -lm

de: de.o eval.o fbgsim.o mersenne.o
	$(CC) $(CFLAGS) *.o -o de $(LDFLAGS)

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<

clean:
	rm -f *.o de
