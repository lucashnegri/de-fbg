#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "mersenne.h"
#include "eval.h"
#include "defs.h"

static double limits[N_DIM][2];

// population and fitness
static double pop  [N_POP][N_DIM]; // current population
static double trial[N_POP][N_DIM]; // trial population
static double fit[N_POP];          // current fitness
static double trial_fit[N_POP];    // fitness of the trial population
double total_fit;
int best;

const char* output_folder;

/**
 * DE/rand/1/bin implementation with bounce back to force the limits.
 */

/* returns a pseudo-random double value between [l,u] */
double rnd_d(double l, double u) {
    return rand_double() * (u-l) + l;
}

/* returns a pseudo-random integer value between [0,N-1] */
int rnd_i(int n) {
    return rand_long() % n;
}

/* initializes the population with random values */
void initialization() {
    best = 0;
    
    for(int p = 0; p < N_POP; ++p) {
        for(int d = 0; d < N_DIM; ++d)
            pop[p][d] = rnd_d(limits[d][0], limits[d][1]);
        
        fit[p] = fitness(pop[p]);
        if(fit[p] > fit[best]) best = p;
    }
}

// DE/rand/1/bin
void step(int it) {
    total_fit = 0;
    
    // generate the trial population
    for(int p = 0; p < N_POP; ++p) {
        int r0, r1, r2;
        do r0 = rnd_i(N_POP); while (r0 == p );
        do r1 = rnd_i(N_POP); while (r1 == r0);
        do r2 = rnd_i(N_POP); while (r2 == r1);
        
        int j = rnd_i(N_DIM);
        for(int d = 0; d < N_DIM; ++d) {
            if( rnd_d(0,1) < Cr || d == j ) {
                trial[p][d] = pop[r0][d] + F*(pop[r1][d] - pop[r2][d]);
    
                if(trial[p][d] < limits[d][0]) trial[p][d] = limits[d][0];
                else if(trial[p][d] > limits[d][1]) trial[p][d] = limits[d][1];
    
                // bounce-back method (when the bonduary is violated)
                if(trial[p][d] < limits[d][0])
                    trial[p][d] += rnd_d(0,1) * limits[d][0] - trial[p][d];
                else if(trial[p][d] > limits[d][1])
                    trial[p][d] += rnd_d(0,1) * limits[d][1] - trial[p][d];
            }
            else
                trial[p][d] = pop[p][d];
        }
    }
    
    // compute the fitness of the trial population
    #pragma omp parallel for
    for(int p = 0; p < N_POP; ++p)
        trial_fit[p] = fitness(trial[p]);
    
    // select the next generation
    for(int p = 0; p < N_POP; ++p) {
        total_fit += trial_fit[p];
        // if the trial is better (or equal to move out of a flat region)
        // than the original, select it
        if(trial_fit[p] > fit[p]) {
            fit[p] = trial_fit[p];
            for(int d = 0; d < N_DIM; ++d)
                pop[p][d] = trial[p][d];
            
            if(trial_fit[p] > fit[best])
                best = p;
        }
    }
    
    // end of iteration callback
    end_step(it, pop[best], fit[best], output_folder);
}

void run() {
    end_step(0, pop[best], fit[best], output_folder);
    
    for(int it = 1; it <= N_IT; ++it) {
        step(it);
        printf("%d %g %g ", it, fit[best], total_fit / N_POP);
        for(int d = 0; d < N_DIM; ++d)
            printf(" %lf", pop[best][d]);
        printf("\n");
    }
}

int main(int argc, char* argv[]) {
    if(argc != 3) {
        printf("Usage: %s exp output_folder\n", argv[0]);
        return 0;
    }
    
    for(int i = 0; i < NSEGS; ++i) {
        limits[i][0] = 0;
        limits[i][1] = 2;
    }
    
    ExperimentType exp = atoi(argv[1]);
    output_folder = argv[2];
    
    /* generate the target FBG/strain distribution */
    init_eval(exp, output_folder);
    
    /* initialize the algorithm */
    unsigned long seed;
    FILE* rnd = fopen("/dev/urandom", "r");
    size_t len = fread(&seed, sizeof(unsigned long), 1, rnd);
    if(len != 1) {
        fprintf(stderr, "Could not read random seed.\n");
        return 0;
    }
    printf("# seed: %lu\n", seed);
    fclose(rnd);
    
    sgenrand(seed);
    initialization();
    
    /* run the algorithm */
    run();
    
    /* end evaluation */
    end_eval(pop[best], output_folder);
    
    return 0;
}
