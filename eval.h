#ifndef EVAL_H
#define EVAL_H

typedef enum {
    ExpRising = 1,
    ExpDecreasing,
    ExpTriangular
} ExperimentType;

void   init_eval(ExperimentType exp, const char* output_folder);
void   end_step (int it, double profile[], double fit, const char* output_folder);
void   end_eval (double computed[], const char* output_folder);

double fitness  (double x[]);

#endif
