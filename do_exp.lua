#! /usr/bin/env lua5.1

local repetitions = 30
local experiments = {1, 2, 3}

local sf = string.format
os.execute('make')
os.execute('mkdir -p exp')

local initial_time = os.time()
local times        = 0

for _, exp in ipairs(experiments) do
    for rep = 1, repetitions do
        times = times + 1
        --cooldown()
        local output = sf("exp/%d/%d", exp, rep)
        os.execute(sf('mkdir -p %s', output))
        print(sf("Starting experiment %d (%02d/%02d)", exp, rep, repetitions))
        local cmd = sf("time ./de %d %s > %s/conv", exp, output, output)
        print('->', cmd)
        os.execute( cmd )
    end
end

local total = os.time() - initial_time
print( sf('%d runs in %f seconds!', times, total) )
