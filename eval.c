#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "eval.h"
#include "fbgsim.h"
#include "defs.h"

double chirp_step = (CHIRP_END-CHIRP_INIT)/ (NPOINTS-1);

static double target_strain[NSEGS]; // in 10^-3
static double spectrum_chirp[NPOINTS];

void do_strain(FBG* fbg, double profile[]) {
    for(int i = 0; i < NSEGS; ++i) {
        FBGSegment* seg = fbg->segments+i;
        double strain = profile[i] * 1e-3;
        seg->length *= 1+strain;
        seg->Lambda *= 1+strain;
        double n03 = seg->n_av*seg->n_av*seg->n_av;
        seg->n_av -= 0.5*n03*(p12 - vf*(p11 + p12))*strain;
    }
}

void create_fbg(FBG** chirp) {
    FBG* fbg = fbg_uniform(LENGTH, LAMBDA_CHIRP, N_AV, DELTA_N0, NSEGS);
    fbg_linear_chirp(fbg, DELTA_CHIRP, 1);
    fbg_apod_gaussian(fbg, ALPHA, GAMMA);
    *chirp = fbg;
}

void print_spectrum(double profile[], const char* path) {
    FILE* f = fopen(path, "w");
    if(!f) fprintf(stderr, "Could not open file %s\n", path);

    FBG *comp_chirp;
    create_fbg(&comp_chirp);
    do_strain(comp_chirp, profile);

    // print resulting spectrum
    for(double lambda = CHIRP_INIT; lambda < CHIRP_END; lambda += 1e-3) {
        double resp   = fbg_sim(comp_chirp, lambda);
        fprintf(f, "%lf %lf\n", lambda, resp);
    }

    fclose(f);
    fbg_free(comp_chirp);
}

/**
 * Creates the strain profile based on the experiment parameter.
 */
void build_target_strain(ExperimentType exp, const char* output_folder) {
    int K = floor(NSEGS/2);

    switch(exp) {
    case ExpRising:
        for(int i = 1; i <= NSEGS; ++i)
            target_strain[i-1] = 0.5 + 0.05*i;
        break;

    case ExpDecreasing:
        for(int i = 1; i <= NSEGS; ++i)
            target_strain[i-1] = 1.5 - 0.05*i;
        break;

    case ExpTriangular:
        for(int i = 1; i <= K; ++i)
            target_strain[i-1] = 0.5 + 0.1*i;

        target_strain[K] = target_strain[K-1];

        for(int i = K+2; i <= NSEGS; ++i)
            target_strain[i-1] = target_strain[K] - 0.1*(i-K-1);
        break;
    }

    static char fname[100];
    snprintf(fname, 100, "%s/target_strain", output_folder);
    FILE* f = fopen(fname, "w");
    if(!f) fprintf(stderr, "Could not open file %s\n", fname);
    for(int i = 0; i < NSEGS; ++i)
        fprintf(f, "%lf\n", target_strain[i]);
    fclose(f);
}

/**
 * Init & end
 */
void init_eval(ExperimentType exp, const char* output_folder) {
    /* create the target FBG */
    FBG *target_chirp;
    create_fbg(&target_chirp);

    /* strain the target FBG */
    build_target_strain(exp, output_folder);
    do_strain(target_chirp, target_strain);

    /* compute the spectrum of the target FBG */
    for(int it = 0; it < NPOINTS; ++it) {
        double lambda = CHIRP_INIT + it*chirp_step;
        spectrum_chirp[it] = fbg_sim(target_chirp, lambda);
    }

    /* we do not need the original FBG anymore */
    fbg_free(target_chirp);
}

/**
 * Fitness evaluation.
 */
double fitness(double x[]) {
    FBG *comp_chirp;
    create_fbg(&comp_chirp);
    do_strain(comp_chirp, x);

    double acc  = 0;

    for(int it = 0; it < NPOINTS; ++it) {
        double lambda = CHIRP_INIT + it*chirp_step;
        double err    = spectrum_chirp[it] - fbg_sim(comp_chirp, lambda);
        acc += err*err;
    }

    // forces a certain smoothness on the strain profile, expected in
    // in usual applications
    for(int s = 1; s < NSEGS; ++s) {
        double err = fabs(x[s] - x[s-1]);
        if(err > SMOOTH_P)
            err = (err - SMOOTH_P);
        else
            err = 0;

        acc += err * SMOOTH_WEIGHT;
    }

    fbg_free(comp_chirp);

    return -acc / NPOINTS;
}

/**
 * End of step
 */
void end_step(int it, double profile[], double fit, const char* output_folder) {
    // every 100 iterations, print the best solution and the resulting spectra
    if( (it % 100) == 0 ) {
        static char fname[100];

        // print strain profile
        {
            snprintf(fname, 100, "%s/profile_%04d", output_folder, it);
            FILE* f = fopen(fname, "w");
            if(!f) fprintf(stderr, "Could not open file %s\n", fname);
            fprintf(f, "#fitness = %lf\n", fit);
            for(int i = 0; i < NSEGS; ++i)
                fprintf(f, "%lf\n", profile[i]);
            fclose(f);
        }
        // print spectra
        {
            snprintf(fname, 100, "%s/spectra_%04d", output_folder, it);
            print_spectrum(profile, fname);
        }
    }
}

void end_eval(double computed[], const char* output_folder) {
    static char fname[100];
    snprintf(fname, 100, "%s/target_spectra", output_folder);
    print_spectrum(target_strain, fname);

    snprintf(fname, 100, "%s/computed_spectra", output_folder);
    print_spectrum(computed, fname);
}
