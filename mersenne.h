#ifndef MERSENNE_H
#define MERSENNE_H

void          sgenrand(unsigned long seed);
void          lsgenrand(unsigned long seed_array[]);
unsigned long rand_long();
double        rand_double();

#endif