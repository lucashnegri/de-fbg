#ifndef DEFS_H
#define DEFS_H

#include "fbgsim.h"

/**
 * FBG parameters
 */
#define LENGTH       1*FBG_CM
#define LAMBDA_CHIRP 0.5324e3f
#define N_AV         1.457f
#define NSEGS        20
#define DELTA_N0     2.5e-4f
#define DELTA_CHIRP  1.0648e-4f
#define ALPHA        10.f
#define GAMMA        0.f

// DELTA_CHIRP was choosen to correspond to a 2nm/cm chirp

/**
 * Computation of strain effects on the fiber.
 * See "Bragg intragrating structural sensing", S. Huang, M. LeBlanc,
 * M. M. Ohn, and R. M. Measures - 1995
 */
#define p11 0.113f
#define p12 0.252f
#define vf  0.17f

#define CHIRP_INIT    1552.0f
#define CHIRP_END     1559.0f
#define SMOOTH_P      0.2f
#define SMOOTH_WEIGHT 10.f
#define NPOINTS       64

// DE parameters
#define N_IT  2000
#define N_POP 100
#define N_DIM NSEGS

/* crossover rate  */
#define Cr 0.95

/* mutation factor */
#define F  0.7     

#endif
