--[[
    Computes the error stats.
--]]

local nexps = 3
local nruns = 30

function read_profile(expm, run, file)
    local path = string.format("exp/%d/%d/%s", expm, run, file)
    local f    = io.open(path)
    
    local values = {}
    
    while true do
        local val = f:read('*l')
        if not val then break end
        local nb  = tonumber(val)
        if nb then -- to ignore comments
            table.insert(values, nb)
        end
    end
    
    f:close()
    
    return values
end

function error_run(expm, run)
    local target   = read_profile(expm, run, "target_strain")
    local computed = read_profile(expm, run, "profile_2000" )
    
    local acc, n = 0, 0
    for i = 1, #computed do
        n = n + 1
        acc = acc + math.abs( computed[i] - target[i] )
    end
    
    return acc / n
end

function sum(x)
    local sum = 0
    
    for i = 1, #x do
        sum = sum + x[i]
    end
    
    return sum
end

function mean(x)
    return sum(x) / #x
end

function std(tbl)
    if #tbl == 1 then return tbl[1], 0 end

    local mean = mean(tbl)
    local sum  = 0
    
    for i, j in ipairs(tbl) do
        local tmp = mean - j
        sum = sum + tmp * tmp
    end
    
    local std = math.sqrt(sum / (#tbl - 1) )
    return mean, std
end

-- returns the mean error and the sample standard deviation
function exp_stats(expm)
    local data = {}
    
    for run = 1, nruns do
        table.insert(data, error_run(expm, run))
        if data[run] > 0.1 then
            print(data[run], run)
        end
    end
    
    return std(data)
end

-- results in microstrain
for e = 1, nexps do
    local mean, std = exp_stats(e)
    
    print(string.format('Experiment %d => mean error: %g, sample std: %g',
        e, mean*1e3, std*1e3))
end
